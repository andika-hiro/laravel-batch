<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/register', [AuthController::class, 'register']);

Route::get('/welcome', [AuthController::class, 'welcome']);

Route::post('/kirim', [AuthController::class, 'kirim']);

Route::get('/cast', [CastController::class, 'index']);

Route::get('/cast/create', [CastController::class, 'create']);

Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast/{cast_id}', [CastController::class, 'show']);

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

Route::put('/cast/{cast_id}', [CastController::class, 'update']);

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

Route::get('/master', function () {
    return view('layout.master');
});

Route::get('/table', function () {
    return view('layout.table');
});

Route::get('/data-tables', function () {
    return view('layout.data-tables');
});
