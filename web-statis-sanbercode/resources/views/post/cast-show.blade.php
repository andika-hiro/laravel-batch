@extends('layout.master-template')

@section('page-title')
{{$cast->nama}}
@endsection

@section('judul')
    {{$cast->nama}}
@endsection

@section('card-title')
{{$cast->nama}}
@endsection


@section('content')
<h2>Show Cast {{$cast->id}}</h2>
<h4>{{$cast->nama}}</h4>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
@endsection