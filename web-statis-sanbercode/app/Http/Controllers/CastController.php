<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('post.cast-index', compact('cast'));
    }

    public function create()
    {
        return view('post.cast-create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('post.cast-show', compact('cast'));
    }


    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('post.cast-edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast,nama,' . $id,
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }


    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
