@extends('layout.master-template')

@section('page-title')
    Edit {{$cast->nama}}
@endsection

@section('judul')
Edit {{$cast->nama}}
@endsection

@section('card-title')
Edit {{$cast->nama}}
@endsection


@section('content')
<div>
    <h2>Edit Cast {{$cast->id}}</h2>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" value="{{$cast->umur}}" name="umur" id="umur" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label><br>
            <textarea name="bio" id="bio" cols="60" rows="5" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection