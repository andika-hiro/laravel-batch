@extends('layout.master-template')

@section('page-title')
    Register
@endsection

@section('judul')
    Register
@endsection

@section('content')
@csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/kirim" method="POST">
            <label for="firstname">First Name:</label><br><br>
            <input type="text" name="firstname" id="firstname" required><br><br>
            <label for="lastname" >Last Name:</label><br><br>
            <input type="text" name="lastname" id="lastname" required><br><br>
    
            <label for="gender">Gender : </label><br><br>
            <input type="radio" name="gender" id="male" value="male" checked> Male<br>
            <input type="radio" name="gender" id="female" value="female"> Female<br>
            <input type="radio" name="gender" id="other" value="other"> Other<br><br>
    
            <label for="nationality">Nationality:</label><br><br>
            <select name="nationality" id="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="singaporean">Singaporean</option>
                <option value="malaysian">Malaysian</option>
                <option value="australian">Australian</option>
            </select><br><br>
    
            <label for="language">Language Spoken:</label><br><br>
            <input type="checkbox" name="language[]" value="0" id="bahasaindonesia">Bahasa Indonesia <br>
            <input type="checkbox" name="language[]" value="1" id="english">English <br>
            <input type="checkbox" name="language[]" value="2" id="other">Other <br> <br>
            
            <label for="bio">Bio:</label><br><br>
            <textarea name="bio" id="bio" cols="30" rows="10" required></textarea><br>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="submit" value="kirim">
        </form>
@endsection



