<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    function register()
    {
        return view('auth.register');
    }
    function welcome()
    {
        return view('auth.welcome');
    }

    function kirim(Request $request)
    {
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];

        return view('auth.welcome', ['firstname' => $firstname, 'lastname' => $lastname, 'gender' => $gender, 'nationality' => $nationality, 'language' => $language, 'bio' => $bio]);
    }
}
