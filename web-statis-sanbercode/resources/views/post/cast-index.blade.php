@extends('layout.master-template')

@section('page-title')
    Cast
@endsection

@section('judul')
    Cast
@endsection

@section('card-title')
    Cast
@endsection

@push('scripts')
<script>
  $(document).ready( function () {
    $('#example1').DataTable();
} );
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.13.2/datatables.min.js"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.2/datatables.min.css"/>
@endpush

@section('content')
<a href="/cast/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th scope="col" >Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr class="border-bottom">
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td class="w-10">
                    <a href="/cast/{{$value->id}}" class="btn btn-info w-100">Show</a>
                    <div class="flex-action d-flex">
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary w-50 my-1">Edit</a>
                        <form action="/cast/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1 w-100 ml-1" value="Delete">
                        </form>
                    </div>
                    
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection